FROM openjdk:11.0.10

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY ./build/libs /usr/src/app

EXPOSE 8080

ENTRYPOINT java -jar devops_essentials_gradle-1.0-SNAPSHOT.jar
